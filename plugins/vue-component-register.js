import Vue from 'vue'

// Bootstrap Vue
import { Layout } from 'bootstrap-vue/es/components'
import { Button } from 'bootstrap-vue/es/components'
import { Image } from 'bootstrap-vue/es/components'
import { Modal } from 'bootstrap-vue/es/components'
import { FormGroup } from 'bootstrap-vue/es/components'
import { FormInput } from 'bootstrap-vue/es/components'
import { Navbar } from 'bootstrap-vue/es/components'
import { Link } from 'bootstrap-vue/es/components'

// Font Awesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { faEdit } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import VueAnalytics from 'vue-analytics'

export default function ({ app: { router } }) {
  // Bootstrap Vue
  Vue.use(Layout)
  Vue.use(Button)
  Vue.use(Image)
  Vue.use(Modal)
  Vue.use(FormGroup)
  Vue.use(FormInput)
  Vue.use(Navbar)
  Vue.use(Link)

  // Font Awesome Icon本体
  Vue.component('ficon', FontAwesomeIcon)

  // Font Awesome Iconのアイコン登録
  library.add(faEdit)

  // Google Analytics
  Vue.use(VueAnalytics, {
    id: 'UA-124252039-1',
    router,
    autoTracking: {
      exception: true
    },
    debug: {
      sendHitTask: process.env.NODE_ENV === 'production' && localStorage['ga_exclude_domikoapp'] != '1'
    }
  })
}
