module.exports = {
  mode: 'spa',
  build: {
    vendor: [
      'nuxt-property-decorator'
    ]
  },
  modules: [
    '~/modules/typescript'
  ],
  plugins: [
    '~/plugins/vue-component-register'
  ],
  head: {
    titleTemplate: (titleChunk) => {
      // タイトル未設定の場合は「サイコロの旅」とだけ表示し
      // タイトル設定済みの場合は「XXX - サイコロの旅」と表示する
      return titleChunk ? `${titleChunk} - サイコロの旅` : 'サイコロの旅'
    },
    meta: [
      { name: 'viewport', content: 'width=device-width, initial-scale=1' }
    ],
    link: [
      { rel: 'icon', type: 'image/vnd.microsoft.icon', sizes: '32x32', href: '/favicon.ico' },
      { rel: 'shortcut icon', href:'/favicon.ico', type:'image/vnd.microsoft.icon'},
      { rel: 'apple-touch-icon', href:'/apple-touch-icon-152x152.png', sizes: '152x152'},
      { rel: 'icon', href:'/android-chrome-192x192.png', type:'image/png', sizes: '192x192'}
    ]
  },
  css: [
    '~/assets/scss/dice-travel.scss'
  ]
}
